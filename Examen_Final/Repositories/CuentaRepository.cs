﻿using Examen_Final.BDD;
using Examen_Final.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Repositories
{
    public interface ICuentaRepository
    {
        void CreateCuenta(Cuenta cuenta);
        void UpdateCuenta(Cuenta cuenta);
        void CreateIngreso(Ingreso ingreso);
        void CreateGasto(Gasto gasto);
        List<Cuenta> getCuentas();
        Cuenta FindCuentaByName(string nombre);
        int GetSaldoCuenta(int id);
        List<Gasto> GetGastosByCuentaId(int id);
        int GetGastoTotalByCuentaId(int id);
        List<Ingreso> GetIngresosByCuentaId(int id);
        int GetIngresoTotalByCuentaId(int id);
    }

    public class CuentaRepository : ICuentaRepository
    {
        private readonly FinanzasContext context;

        public CuentaRepository(FinanzasContext context)
        {
            this.context = context;
        }

        public void CreateCuenta(Cuenta cuenta)
        {
            context.cuentas.Add(cuenta);
            context.SaveChanges();
        }

        public void UpdateCuenta(Cuenta cuenta)
        {
            context.cuentas.Update(cuenta);
            context.SaveChanges();
        }

        public void CreateIngreso(Ingreso ingreso)
        {
            context.ingresos.Add(ingreso);
            context.SaveChanges();
        }

        public void CreateGasto(Gasto gasto)
        {
            context.gastos.Add(gasto);
            context.SaveChanges();
        }

        public List<Cuenta> getCuentas()
        {
            return context.cuentas.ToList();
        }

        public Cuenta FindCuentaByName(string nombre)
        {
            var cuenta = context.cuentas.FirstOrDefault(c => c.nombre == nombre);
            if (cuenta != null)
            {
                return cuenta;
            }
            else return null;
        }

        public int GetSaldoCuenta(int id)
        {
            var cuenta = context.cuentas.FirstOrDefault(c => c.id == id);
            if (cuenta != null)
            {
                return cuenta.sInicial;
            }
            else return 0;
        }

        public List<Gasto> GetGastosByCuentaId(int id)
        {
            var gastos = context.gastos.Where(i => i.id_cuenta == id);
            return gastos.ToList();
        }

        public int GetGastoTotalByCuentaId(int id)
        {
            int total = 0;
            var gastos = context.gastos.Where(i => i.id_cuenta == id).ToList();
            foreach (Gasto gasto in gastos)
            {
                total = total + gasto.monto;
            }
            return total;
        }

        public List<Ingreso> GetIngresosByCuentaId(int id)
        {
            var ingresos = context.ingresos.Where(i => i.id_cuenta == id);
            return ingresos.ToList();
        }

        public int GetIngresoTotalByCuentaId(int id)
        {
            int total = 0;
            var ingresos = context.ingresos.Where(i => i.id_cuenta == id).ToList();
            foreach (Ingreso ingreso in ingresos)
            {
                total = total + ingreso.monto;
            }
            return total;
        }
    }
}
