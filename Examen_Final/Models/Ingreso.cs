﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Models
{
    public class Ingreso
    {
        public int id { get; set; }
        public int id_cuenta { get; set; }
        public string fecha { get; set; }
        public string descripcion { get; set; }
        public int monto { get; set; }
    }
}
