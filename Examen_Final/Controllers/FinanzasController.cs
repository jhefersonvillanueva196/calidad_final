﻿using Examen_Final.BDD;
using Examen_Final.Models;
using Examen_Final.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.Controllers
{
    public class FinanzasController : Controller
    {
        private readonly ICuentaRepository repository;

        public FinanzasController(ICuentaRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        public IActionResult MostrarCuentas()
        {
            return View(repository.getCuentas());
        }
        [HttpGet]
        public IActionResult CrearCuenta()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CrearCuentas(string nombre, string tp, int saldo)
        {
            if (nombre == "" || tp == "" || saldo == 0)
            {
                return View();
            }
            else
            {
                Cuenta cu = new Cuenta
                {
                    nombre = nombre,
                    categoria = tp,
                    sInicial = saldo
                };
                repository.CreateCuenta(cu);
                var liCuentas = repository.getCuentas();
                return RedirectToAction("MostrarCuentas", liCuentas);
            }
            
        }

        [HttpGet]
        public IActionResult AgregarGastos()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AgregarGasto(string nombre, string fecha, string descripcion, int saldo)
        {
            Cuenta cuenta = repository.FindCuentaByName(nombre);
            var diferencia = cuenta.sInicial - saldo;
            if (nombre == "" || fecha == "" || descripcion == "" || saldo == 0)
            {
                return View("AgregarGastos");
            }
            else
            {
                if (diferencia < 0 && cuenta.categoria == "Propio")
                {
                    return View("AgregarGastos");
                }
                else
                {
                    cuenta.sInicial = cuenta.sInicial - saldo;
                    Gasto gasto = new Gasto
                    {
                        id_cuenta = cuenta.id,
                        descripcion = descripcion,
                        fecha = fecha,
                        monto = saldo,
                    };
                    repository.UpdateCuenta(cuenta);
                    repository.CreateGasto(gasto);
                    var liCuentas = repository.getCuentas();
                    return RedirectToAction("MostrarCuentas", liCuentas);
                }
            }
        }

        [HttpGet]
        public IActionResult AgregarIngresos()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AgregarIngreso(string nombre, string fecha, string descripcion, int saldo)
        {
            if (nombre == "" || fecha == "" || descripcion == "" || saldo == 0)
            {
                return View("AgregarGastos");
            }
            else
            {
                Cuenta cuenta = repository.FindCuentaByName(nombre);
                cuenta.sInicial = cuenta.sInicial + saldo;
                Ingreso ingreso = new Ingreso
                {
                    id_cuenta = cuenta.id,
                    descripcion = descripcion,
                    fecha = fecha,
                    monto = saldo,
                };
                repository.UpdateCuenta(cuenta);
                repository.CreateIngreso(ingreso);
                var liCuentas = repository.getCuentas();
                return RedirectToAction("MostrarCuentas", liCuentas);
            }
            
        }

        [HttpGet]
        public IActionResult Gastos(int id)
        {
            var liGastos = repository.GetGastosByCuentaId(id);
            return View("Gastos", liGastos);
        }
        [HttpGet]
        public IActionResult Ingresos(int id)
        {
            var liIngresos = repository.GetIngresosByCuentaId(id);
            return View("Ingresos", liIngresos);
        }
    }
}