﻿using Examen_Final.BDD.Mapping;
using Examen_Final.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_Final.BDD
{
    public class FinanzasContext : DbContext
    {
        public virtual DbSet<Cuenta> cuentas { get; set; }
        public virtual DbSet<Gasto> gastos { get; set; }
        public virtual DbSet<Ingreso> ingresos { get; set; }

        public FinanzasContext(DbContextOptions<FinanzasContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new CuentaMap());
            modelBuilder.ApplyConfiguration(new GastoMap());
            modelBuilder.ApplyConfiguration(new IngresoMap());
        }
    }
}
