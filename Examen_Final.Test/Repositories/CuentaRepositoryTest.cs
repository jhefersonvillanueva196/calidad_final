﻿using Examen_Final.BDD;
using Examen_Final.Repositories;
using Examen_Final.Test.Repositories.Mocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Final.Test.Repositories
{
    public class CuentaRepositoryTest
    {
        private Mock<FinanzasContext> mockContext;

        [SetUp]
        public void SetUp()
        {
            mockContext = FinanzasContextMock.GetFinanzasContextMock();
        }

        #region Cuentas

        [Test]
        public void FindAllCuentas()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var cuentas = repository.getCuentas();

            Assert.NotNull(cuentas);
        }

        [Test]
        public void FindAllCuentas2()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var cuentas = repository.getCuentas();

            Assert.AreEqual(5,cuentas.Count);
        }

        [Test]
        public void FindAllCuentas3()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var cuentas = repository.getCuentas();

            Assert.AreNotEqual(4, cuentas.Count);
        }

        [Test]
        public void FindCuentaByName()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var cuenta = repository.FindCuentaByName("Cuenta 1");

            Assert.IsNotNull(cuenta);
        }

        [Test]
        public void FindCuentaByName2()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var cuenta = repository.FindCuentaByName("Cuenta 1");

            Assert.AreEqual(1,cuenta.id);
        }

        [Test]
        public void FindSaldoByCuentaId()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var cuenta = repository.GetSaldoCuenta(5);

            Assert.AreEqual(1000, cuenta);
        }

        [Test]
        public void FindSaldoByCuentaId2()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var cuenta = repository.GetSaldoCuenta(3);

            Assert.AreNotEqual(1000, cuenta);
        }

        #endregion

        #region Gastos

        [Test]
        public void GetGastosByIdCuenta()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var gasto = repository.GetGastosByCuentaId(2);
            Assert.NotNull(gasto);
        }

        [Test]
        public void GetGastosByIdCuenta2()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var gasto = repository.GetGastosByCuentaId(2);
            Assert.AreEqual(3, gasto.Count);
        }

        [Test]
        public void GetGastosByIdCuenta3()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var gasto = repository.GetGastosByCuentaId(5);
            Assert.AreNotEqual(3, gasto.Count);
        }

        [Test]
        public void GetSumaGastosByIdCuenta()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var gasto = repository.GetGastoTotalByCuentaId(5);
            Assert.AreEqual(450, gasto);
        }

        [Test]
        public void GetSumaGastosByIdCuenta2()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var gasto = repository.GetGastoTotalByCuentaId(3);
            Assert.AreEqual(190, gasto);
        }

        #endregion

        #region Ingresos

        [Test]
        public void GetIngresosByIdCuenta()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var ingreso = repository.GetIngresosByCuentaId(2);
            Assert.NotNull(ingreso);
        }

        [Test]
        public void GetIngresosByIdCuenta2()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var ingreso = repository.GetIngresosByCuentaId(2);
            Assert.AreEqual(2, ingreso.Count);
        }

        [Test]
        public void GetIngresosByIdCuenta3()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var ingreso = repository.GetIngresosByCuentaId(5);
            Assert.AreNotEqual(3, ingreso.Count);
        }

        [Test]
        public void GetSumaIngresosByIdCuenta()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var ingreso = repository.GetIngresoTotalByCuentaId(5);
            Assert.AreEqual(150, ingreso);
        }

        [Test]
        public void GetSumaIngresosByIdCuenta2()
        {
            var repository = new CuentaRepository(mockContext.Object);
            var ingreso = repository.GetIngresoTotalByCuentaId(3);
            Assert.AreEqual(300, ingreso);
        }

        #endregion
    }
}
