﻿using Examen_Final.BDD;
using Examen_Final.Models;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Examen_Final.Test.Repositories.Mocks
{
    public static class FinanzasContextMock
    {
        public static Mock<FinanzasContext> GetFinanzasContextMock()
        {
            IQueryable<Cuenta> cuentaData = GetCuentaData();

            var mockDbSetCuenta = new Mock<DbSet<Cuenta>>();
            mockDbSetCuenta.As<IQueryable<Cuenta>>().Setup(m => m.Provider).Returns(cuentaData.Provider);
            mockDbSetCuenta.As<IQueryable<Cuenta>>().Setup(m => m.Expression).Returns(cuentaData.Expression);
            mockDbSetCuenta.As<IQueryable<Cuenta>>().Setup(m => m.ElementType).Returns(cuentaData.ElementType);
            mockDbSetCuenta.As<IQueryable<Cuenta>>().Setup(m => m.GetEnumerator()).Returns(cuentaData.GetEnumerator());
            mockDbSetCuenta.Setup(m => m.AsQueryable()).Returns(cuentaData);


            IQueryable<Ingreso> ingresoData = GetIngresoData();

            var mockDbSetIngreso = new Mock<DbSet<Ingreso>>();
            mockDbSetIngreso.As<IQueryable<Ingreso>>().Setup(m => m.Provider).Returns(ingresoData.Provider);
            mockDbSetIngreso.As<IQueryable<Ingreso>>().Setup(m => m.Expression).Returns(ingresoData.Expression);
            mockDbSetIngreso.As<IQueryable<Ingreso>>().Setup(m => m.ElementType).Returns(ingresoData.ElementType);
            mockDbSetIngreso.As<IQueryable<Ingreso>>().Setup(m => m.GetEnumerator()).Returns(ingresoData.GetEnumerator());
            mockDbSetIngreso.Setup(m => m.AsQueryable()).Returns(ingresoData);

            IQueryable<Gasto> gastodata = GetGastoData();

            var mockDbSetGasto = new Mock<DbSet<Gasto>>();
            mockDbSetGasto.As<IQueryable<Gasto>>().Setup(m => m.Provider).Returns(gastodata.Provider);
            mockDbSetGasto.As<IQueryable<Gasto>>().Setup(m => m.Expression).Returns(gastodata.Expression);
            mockDbSetGasto.As<IQueryable<Gasto>>().Setup(m => m.ElementType).Returns(gastodata.ElementType);
            mockDbSetGasto.As<IQueryable<Gasto>>().Setup(m => m.GetEnumerator()).Returns(gastodata.GetEnumerator());
            mockDbSetGasto.Setup(m => m.AsQueryable()).Returns(gastodata);


            var mockContext = new Mock<FinanzasContext>(new DbContextOptions<FinanzasContext>());
            mockContext.Setup(c => c.cuentas).Returns(mockDbSetCuenta.Object);
            mockContext.Setup(c => c.ingresos).Returns(mockDbSetIngreso.Object);
            mockContext.Setup(c => c.gastos).Returns(mockDbSetGasto.Object);

            return mockContext;
        }

        private static IQueryable<Cuenta> GetCuentaData()
        {
            return new List<Cuenta>
            {
                new Cuenta { id=1,nombre="Cuenta 1",categoria="Propio",sInicial=2500 },
                new Cuenta { id=2,nombre="Cuenta 2",categoria="Credito",sInicial=5000 },
                new Cuenta { id=3,nombre="Cuenta 3",categoria="Credito",sInicial=6000 },
                new Cuenta { id=4,nombre="Cuenta 4",categoria="Propio",sInicial=1500 },
                new Cuenta { id=5,nombre="Cuenta 5",categoria="Propio",sInicial=1000 },
            }.AsQueryable();
        }

        private static IQueryable<Ingreso> GetIngresoData()
        {
            return new List<Ingreso>
            {
                new Ingreso { id=1,id_cuenta=1,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=150},
                new Ingreso { id=2,id_cuenta=1,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=300},
                new Ingreso { id=3,id_cuenta=2,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=100},
                new Ingreso { id=4,id_cuenta=2,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=200},
                new Ingreso { id=5,id_cuenta=3,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=120},
                new Ingreso { id=6,id_cuenta=3,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=180},
                new Ingreso { id=7,id_cuenta=4,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=200},
                new Ingreso { id=8,id_cuenta=4,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=350},
                new Ingreso { id=9,id_cuenta=4,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=180},
                new Ingreso { id=10,id_cuenta=5,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=150},
            }.AsQueryable();
        }

        private static IQueryable<Gasto> GetGastoData()
        {
            return new List<Gasto>
            {
                new Gasto { id=1,id_cuenta=1,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=150},
                new Gasto { id=2,id_cuenta=1,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=150},
                new Gasto { id=3,id_cuenta=2,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=50},
                new Gasto { id=4,id_cuenta=2,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=90},
                new Gasto { id=5,id_cuenta=2,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=50},
                new Gasto { id=6,id_cuenta=3,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=90},
                new Gasto { id=7,id_cuenta=3,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=100},
                new Gasto { id=8,id_cuenta=4,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=200},
                new Gasto { id=9,id_cuenta=5,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=300},
                new Gasto { id=10,id_cuenta=5,fecha=new DateTime(2021, 08,20, 10, 10, 0).ToString(),descripcion="Ingreso",monto=150},
            }.AsQueryable();
        }

    }
}
