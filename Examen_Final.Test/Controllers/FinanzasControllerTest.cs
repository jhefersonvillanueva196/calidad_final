﻿using Examen_Final.Controllers;
using Examen_Final.Models;
using Examen_Final.Repositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_Final.Test.Controllers
{
    public class FinanzasControllerTest
    {
        private Mock<ICuentaRepository> repository;

        [SetUp]
        public void SetUp()
        {
            repository = new Mock<ICuentaRepository>();
        }

        #region Cuenta

        [Test]
        public void CrearCuenta()
        {
            var serviceMock = new Mock<ICuentaRepository>();


            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.CrearCuentas("Cuenta 1", "Propio", 3000);

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }

        [Test]
        public void CrearCuenta2()
        {
            var serviceMock = new Mock<ICuentaRepository>();


            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.CrearCuentas("Cuenta 7", "Propio", 3000);

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }

        [Test]
        public void CrearCuentaFail()
        {
            var serviceMock = new Mock<ICuentaRepository>();


            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.CrearCuentas("Cuenta 1", "Propio", 0);

            Assert.IsInstanceOf<ViewResult>(redirect);
        }

        [Test]
        public void CrearCuentaFail2()
        {
            var serviceMock = new Mock<ICuentaRepository>();

            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.CrearCuentas("Cuenta 1", "", 200);

            Assert.IsInstanceOf<ViewResult>(redirect);
        }

        #endregion

        #region Gastos

        [Test]
        public void CrearGasto()
        {
            var cuenta = new Cuenta { id = 1, nombre = "Cuenta 1", categoria = "Propio", sInicial = 2500 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 1")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 1", "01/12/2021","Prueba de crear gasto",200);

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }

        [Test]
        public void CrearGastoNoLimitCredito()
        {
            var cuenta = new Cuenta { id = 3, nombre = "Cuenta 3", categoria = "Credito", sInicial = 6000 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 3")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 3", "01/12/2021", "Prueba de crear gasto", 6500);

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }

        [Test]
        public void CrearGastoFailNoData()
        {
            var cuenta = new Cuenta { id = 1, nombre = "Cuenta 1", categoria = "Propio", sInicial = 2500 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 1")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 1", "01/12/2021", "", 200);

            Assert.IsInstanceOf<ViewResult>(redirect);
        }

        [Test]
        public void CrearGastoFailNoData2()
        {
            var cuenta = new Cuenta { id = 1, nombre = "Cuenta 1", categoria = "Propio", sInicial = 2500 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 1")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 1", "", "", 200);

            Assert.IsInstanceOf<ViewResult>(redirect);
        }

        [Test]
        public void CrearGastoFailLimitExceded()
        {
            var cuenta = new Cuenta { id = 1, nombre = "Cuenta 1", categoria = "Propio", sInicial = 2500 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 1")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 1", "01/12/2021", "Prueba de crear gasto", 2600);

            Assert.IsInstanceOf<ViewResult>(redirect);
        }

        [Test]
        public void CrearGastoFailLimitExceded2()
        {
            var cuenta = new Cuenta { id = 4, nombre = "Cuenta 4", categoria = "Propio", sInicial = 1500 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 4")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 4", "01/12/2021", "Prueba de crear gasto", 2600);

            Assert.IsInstanceOf<ViewResult>(redirect);
        }

        #endregion

        #region Ingreso

        [Test]
        public void CrearIngreso()
        {
            var cuenta = new Cuenta { id = 1, nombre = "Cuenta 1", categoria = "Propio", sInicial = 2500 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 1")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 1", "01/12/2021", "Prueba de crear ingreso", 200);

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }

        [Test]
        public void CrearIngreso2()
        {
            var cuenta = new Cuenta { id = 3, nombre = "Cuenta 3", categoria = "Credito", sInicial = 6000 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 3")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 3", "01/12/2021", "Prueba de crear ingreso", 200);

            Assert.IsInstanceOf<RedirectToActionResult>(redirect);
        }

        [Test]
        public void CrearIngresoFailNoData()
        {
            var cuenta = new Cuenta { id = 3, nombre = "Cuenta 3", categoria = "Credito", sInicial = 6000 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 3")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 3", "01/12/2021", "", 200);

            Assert.IsInstanceOf<ViewResult>(redirect);
        }

        [Test]
        public void CrearIngresoFailNoData2()
        {
            var cuenta = new Cuenta { id = 3, nombre = "Cuenta 3", categoria = "Credito", sInicial = 6000 };
            var serviceMock = new Mock<ICuentaRepository>();
            serviceMock.Setup(o => o.FindCuentaByName("Cuenta 3")).Returns(cuenta);
            var controller = new FinanzasController(serviceMock.Object);

            var redirect = controller.AgregarGasto("Cuenta 3", "01/12/2021", "Test de No data", 0);

            Assert.IsInstanceOf<ViewResult>(redirect);
        }

        #endregion
    }
}
